# -*-coding:utf-8-*-

alias fixsteam='find ~/.steam/root/ \( -name "libgcc_s.so*" -o -name "libstdc++.so*" -o -name "libxcb.so*" \) -print -delete'
alias winesteam="wine ~/.wine/drive_c/Program\ Files\ \(x86\)/Steam/Steam.exe"

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias sl='ls --color=auto'
alias nohup='nohup > /dev/shm/nohup'
alias egcc='gcc -fdiagnostics-color=auto -std=gnu99 -Wall -Werror -Wextra -Wold-style-cast -pedantic -g3'
alias g++98='g++ -fdiagnostics-color=auto -Wall -Wextra -Weffc++ -Wold-style-cast -Woverloaded-virtual -std=c++98 -pedantic -g3'
alias g++11='g++ -fdiagnostics-color=auto -Wall -Wextra -Weffc++ -Wold-style-cast -Woverloaded-virtual -std=c++11 -pedantic -g3'

alias top="top -o %CPU"
#alias topc="top -u $USER -o %CPU"
alias topc="top -o %CPU"
#alias topm="top -u $USER -o %MIN"
alias topm="top -o %MIN"

alias g++14='g++ -fdiagnostics-color=auto -Wall -Wextra -Weffc++ -Wold-style-cast -Woverloaded-virtual -std=c++11 -pedantic -g3'
alias udisksm='udisksctl mount --block-device'
alias udisksum='udisksctl unmount --block-device'
alias pm='ps -ef|grep -i'
alias wdu='du -h'
alias gh='history|grep -i'
alias free='free -h'
#alias tildeclean='rm -v *~ .*~ \#*\# *.swp .*.swp'
#alias drobboxconflicted='rm -v *conflicted\ copy*'
alias andemu="emulator64-x86 -avd def -qemu -enable-kvm"
alias chromedebug="LANG='en_US.UTF-8' chromium --user-data-dir='/home/tofu/Downloads/chromedev' --disk-cache-dir=/dev/shm/chromedebug --media-cache-dir=/dev/shm/chromedev --proxy-server='http://localhost:8123'"
#alias phoneemu='phonegap run android --emulator'
#alias phonedev='phonegap run android --device'
alias qnix='xrandr --output DVI-1 --mode 2560x1440 --rate 60'
alias highres='xrandr --output LVDS1 --scale 1.25x1.25 --panning 1708x960'
alias lifebookhighres="xrandr --output LVDS1 --scale 1.25x1.25 --panning 1280x960"
alias higherres="xrandr --output LVDS1 --mode 1366x768 --scale 1.5x1.5 --panning 0x0"
alias sharptv='xrandr --output HDMI1 --mode 1280x720 --transform 1,0,-32,0,1.0115,-19,0,0,0.7155 --panning 1709x1019'
alias defaultres='xrandr --output LVDS1 --scale 1x1 --panning 1366x768'
alias emacs='emacs &> /dev/null'
alias gvim='gvim &> /dev/null'
alias blockify='blockify &> /dev/null'
alias rebash='source $HOME/.bashrc'
alias ramdisk='cd /dev/shm'
alias mpvm='mpv --profile=music'
alias tv='xrandr --output HDMI1 --mode 1920x1080 --output LVDS1 --off'
alias lvds='xrandr --output LVDS1 --mode 1366x768 --output HDMI1 --off'
alias hightv='xrandr --output HDMI1 --mode 1920x1080 --scale 1.25x1.25 --panning 2400x1350'
alias deftv='xrandr --output HDMI1 --mode 1920x1080 --scale 1x1 --panning 1920x1008'
alias xztar='tar c -I "xz -T 4"'
alias nmapfind='nmap -sP 192.168.1.1/24'
#alias compton='compton 2> /dev/null'
#alias btaudio="pacmd set-default-source bluez_sink.6C_5A_B5_F6_6E_76.monitor && pacmd set-default-source bluez_sink.6C_5A_B5_F6_6E_76.monitor"
alias adbdev='cd ~/airsoftdb/airsoftdb-recon/airsoftdb/recon/reconengine/spiders'
#alias sway='XKB_DEFAULT_LAYOUT=se XKB_DEFAULT_VARIANT=se XKB_DEFAULT_MODEL=pc105 sway'
#alias sway='XKB_DEFAULT_LAYOUT=se sway'
#alias cyanogen="export PATH='/home/tofu/bin:$PATH'"
alias dontstarve="steam -applaunch 219740"
alias deathmetal="mpv https://www.youtube.com/playlist?list=PL_pdgYibV1rxPzYDvOT-vtUpipU6yZMVP --profile music"
alias gamemusic="mpv https://www.youtube.com/playlist?list=PL_pdgYibV1ryI0tU2PCVOagW8FD-fz939 --profile music"
alias ambient="mpv 'https://www.youtube.com/watch?v=9XyeTLIpAXs&list=PLrz8hHdG8-5BDL03_rOks1gLg-5C58pNp' --profile music"

alias xi3='startx /usr/bin/i3'
alias xlx='startx /usr/bin/startlxde'

alias lsc='ls --color=never'
