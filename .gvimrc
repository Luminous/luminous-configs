"nmap <s-w> :silent !gvim<CR>
"set cursorline
"set cursorcolumn
" Remove toolbar and scrollbars from gVim,
" because, frankly, they are a waste of space.
set guioptions-=T
set guioptions-=r
set guioptions-=l
set guioptions-=m
" Add Vim icon to window, where it is shown depends on platform, windowing
" system, X11 server depth, etc etc.
set guioptions+=i
" Automagically yank to windowing system clipboard on visual select.
" This makes gvim behave like a normal unix application.
set guioptions+=a
" Remove scrollbars
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R
set guioptions-=b
" Use console dialogs instead of popup dialogs, gosh
set guioptions+=c
