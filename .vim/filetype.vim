if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  " au! commands to set the filetype go here
  au! BufRead,BufNewFile *.json set filetype=json
  augroup json_autocmd
    autocmd!
    autocmd FileType json set autoindent
    autocmd FileType json set formatoptions=tcq2l
    autocmd FileType json set textwidth=78 shiftwidth=2
    autocmd FileType json set softtabstop=2 tabstop=2
    autocmd FileType json set expandtab
    autocmd FileType json set foldmethod=syntax
  augroup END
augroup END
