nnoremap <silent> <buffer> <leader>ji :JavaImport<cr>
nnoremap <silent> <buffer> <leader>jK :JavaDocSearch -x declarations<cr>
nnoremap <silent> <buffer> <leader>js :JavaSearchContext<cr>
nnoremap <silent> <buffer> <leader>jm :Javac<cr>
nnoremap <silent> <buffer> <leader>jc :JavaCorrect<cr>
nnoremap <silent> <buffer> <leader>jr :JavaRename 
nnoremap <silent> <buffer> <leader>jf :%JavaFormat<cr>
