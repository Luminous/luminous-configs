---------------------------------------------------------------------

    COLLECTION OF MORE OR LESS USEFUL CONFIGFILES

---------------------------------------------------------------------

This is a collection of configs for various applications and goodies.

The 51-android.rules is for samsung android phones. There are such files on the web with
rules for just about all phone vendors. Mine only has samsung, useful if you develop with
android sdk.

Vim configs are huge and cluttered but somewhat cleaned up. I need to hunt down a bug with underscore characters etc.
Weird stuff...

Neocomplete plugin is a must too, I just wish I knew how to make it complete on TAB, instead of ctrl+n/p

Compton is great, config is pretty complete. Although it makes i3s window-title-bars disappear, so I wouldn't recommend it with i3.
Try fiddling with the settings, I think it's comptons fault but a config fix possible.

pentadactyl was found recently by me, didn't know vimp had a fork. Best addon ever, and better than any of all other hint-style browser combined.
Possibly not dwb, but that one bugged me terribly in a litterary fashion.

Also, I switched awesome for i3 recently, now has a config. It's good, but SLUGGISH compared to openbox/lxde.

I love lxde/openbox, sadly it's stacked :(
