;; collection of emacs settings collected from various sources
;; Base file: tao@ida, tjo@ida
;; Additions: klaar@ida, Jonathand Waldsjö, jonwa416@student

;; Save this file in your home folder (~/.emacs)
;; If you dislike som feature, just make it a comment like this

;; You can use xfontsel or xlsfonts to find a font you like
;; You get a lot faster and nicer startup appearance if you set the
;; font on the command line, make an alias instead:
;; alias emacs 'emacs -fn "-*-fixed-*-r-*-*-12-*-*-*-*-*-iso8859-*"'
;(set-default-font "-*-fixed-*-r-normal-*-12-*-*-*-*-*-iso8859-*")

;; No annoying messages at startup, thank you very much.
(setq inhibit-default-init t)
(setq inhibit-startup-message t)
(custom-set-variables
 '(column-number-mode t)
 '(inhibit-startup-screen t)
 '(show-paren-mode t))

;; Make good use of your pixels by disabling some useless graphics
(tool-bar-mode nil)
(menu-bar-mode nil)
(scroll-bar-mode nil)

;; Put the scrollbar where it should be
;(setq scroll-bar-mode-explicit t)
;(set-scroll-bar-mode 'right)

;; Highlight matching parenthesis
(load "paren")
(show-paren-mode 1)

(global-font-lock-mode t)                      ; Highlighting...
(setq font-lock-maximum-decoration t)          ; ...as much as possible...
;(setq font-lock-support-mode 'lazy-lock-mode)  ; ...in the background (?)...
;(setq lazy-lock-stealth-time 30)               ; ...after 30 seconds...
;(setq lazy-lock-stealth-verbose t)             ; ...and report doing so...
;(setq lazy-lock-defer-driven 1)                ; ...defer-driven "eventually".

;; Better suport for Swedish characters ( why would you use them ? )
;(set-language-environment 'UTF-8')
;(set-terminal-coding-system 'UTF-8')

;; Base colors for black background (this rocks!)
(set-background-color "#2f4f4f")
(set-foreground-color "White")
(set-cursor-color     "ForestGreen")

;; Base colors for white background
;(set-background-color "White")
;(set-foreground-color "Black")
;(set-cursor-color     "ForestGreen")

;; ----------------------------------------------------------------------------
;; Några extra saker som kan vara trevliga att ha.
;; ----------------------------------------------------------------------------

;; Hantera character med kod 128+ (t. ex. svenska vokaler)
(set-input-mode (car (current-input-mode))
  (nth 1 (current-input-mode))
  0)

;; Delete-knapp raderar all text i ett markerat område
(delete-selection-mode t)

;; Invertera markerat område
;(transient-mark-mode t)

;; "Ctrl-RET" för att fylla ut ett ord (ungefär som "TAB" i skalfönstret).
(global-set-key '[(control return)] 'dabbrev-expand)

;; Se till att man inte kan gå längre ner i filen än till sista raden
;; Gör att man inte får en massa tomma rader i slutet av filen
(setq next-line-add-newlines nil)

;; Gör så att man endast "scrollar" en rad i taget när man går uppåt
;; och neråt
(setq scroll-step 1)
(setq scroll-conservatively 1)

;; Se till att vi håller reda på vilken rad och position på rad vi
;; befinner oss på.
(setq line-number-mode t)
(setq column-number-mode t)

;; Byt ordning på CR/LF Kommer att ge indrag på varje ny rad!
(setq foo (global-key-binding "\C-m"))              ; Spara CF
(global-set-key "\C-m" (global-key-binding "\C-j")) ; Sätt CR till LF
(global-set-key "\C-j" foo)                         ; Sätt LF till gamla CR

;; F5 ändrar "line-wrap".
(global-set-key [f5] 'toggle-truncate-lines)

;; C++ specifikt
;; Behandla *.h-filer som C++ ist.f. C
(add-to-list 'auto-mode-alist '("\\.h$" . c++-mode))

;; Bra indrag
(setq c-default-style "ellemtel")
(setq-default indent-tabs-mode nil)

;; Djupet på varje indenteringsnivå
(setq c-basic-offset 2)
(setq c-indent-level 2)
(global-linum-mode 1) ; Radnumrering
(global-hl-line-mode 1)
(set-face-background 'hl-line "#3e4446")

;(add-to-list 'load-path "~/.elisp/auto-complete-1.3.1")
;(setq ac-dictionary-directories "~/.elisp/auto-complete-1.3.1/dict")
;(require 'auto-complete-config)
;(ac-config-default)
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
  '(default ((t (:inherit nil :stipple nil :background "#222222" :foreground "#aaeebb" :inverse-video nil :box nil :strfike-through nil :overline nil :underline nil :slant normal :weight normal :height 90 :width normal :foundry "unknown" :family "Ubuntu Mono")))))
;  '(default ((t (:inherit nil :stipple nil :background "#777777" :foreground "#FFFFFF" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 90 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))

  (add-to-list 'auto-mode-alist
                              '("\\.\\(?:cap\\|gemspec\\|irbrc\\|gemrc\\|rake\\|rb\\|ru\\|thor\\)\\'" . ruby-mode))
  (add-to-list 'auto-mode-alist
                              '("\\(?:Brewfile\\|Capfile\\|Gemfile\\(?:\\.[a-zA-Z0-9._-]+\\)?\\|[rR]akefile\\)\\'" . ruby-mode))
