set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'

Plugin 'Valloric/YouCompleteMe'

"
"" YouCompleteMe options
"
"
let g:ycm_register_as_syntastic_checker = 1 "default 1
let g:Show_diagnostics_ui = 1 "default 1
"
""will put icons in Vim's gutter on lines that have a diagnostic set.
"Turning this off will also turn off the YcmErrorLine and YcmWarningLine
""highlighting
let g:ycm_enable_diagnostic_signs = 1
let g:ycm_enable_diagnostic_highlighting = 1
let g:ycm_always_populate_location_list = 1 "default 0
let g:ycm_open_loclist_on_ycm_diags = 1 "default 1

let g:ycm_complete_in_strings = 1 "default 1
let g:ycm_collect_identifiers_from_tags_files = 0 "default 0
let g:ycm_path_to_python_interpreter = '' "default ''

let g:ycm_server_use_vim_stdout = 0 "default 0 (logging to console)
let g:ycm_server_log_level = 'info' "default info

let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'  "where to search for .ycm_extra_conf.py if not found
let g:ycm_confirm_extra_conf = 1

let g:ycm_goto_buffer_command = 'same-buffer' "[ 'same-buffer', 'horizontal-split', 'vertical-split', 'new-tab' ]
let g:ycm_filetype_whitelist = { '*': 1 }
let g:ycm_key_invoke_completion = '<C-Space>'


nnoremap <F11> :YcmForceCompileAndDiagnostics <CR>")

" Ultisnips
"Plugin 'SirVer/ultisnips'

"snippets
"Plugin 'honza/vim-snippets'

"syntastic
Plugin 'scrooloose/syntastic'
let g:syntastic_python_checkers = ['pylint', 'flake8']
"let g:syntastic_quiet_messages = {
"    \ "!level":   "errors",
"    \ "type":   "style",
"    \ "regex":   ".*",
"    \ "file:p":   ".*"}

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}

"Plugin 'Chiel92/vim-autoformat'

"closetag
"Plugin 'alvan/vim-closetag'

" filenames like *.xml, *.html, *.xhtml, ...
let g:closetag_filenames = "*.html,*.xhtml,*.phtml"

"ctrlp
"Plugin 'kien/ctrlp.vim'

"delimitmate
"Plugin 'Raimondi/delimitMate'

"html5
"Plugin 'othree/html5.vim'

"lightline
"Plugin 'itchyny/lightline.vim'

"let g:lightline = {
"      \ 'colorscheme': 'solarized',
"      \ }
"set laststatus=2

"molokai
"Plugin 'tomasr/molokai'

"vim-colors-solarized
"Plugin 'altercation/vim-colors-solarized'

"vim-kolor
"Plugin 'zeis/vim-kolor'

let g:kolor_italic=1                    " Enable italic. Default: 1
let g:kolor_bold=1                      " Enable bold. Default: 1
let g:kolor_underlined=0                " Enable underline. Default: 0
let g:kolor_alternative_matchparen=0    " Gray 'MatchParen' color. Default: 0

"morhetz/gruvbox
"Plugin 'morhetz/gruvbox'

"vim-vividchalk
"Plugin 'tpope/vim-vividchalk'

"vim-distinguished
"Plugin 'Lokaltog/vim-distinguished'

"mango
"Plugin 'goatslacker/mango.vim'

"command_t
"Plugin 'wincent/command-t'

"mustache
"Plugin 'mustache/vim-mustache-handlebars'

"nerdcommenter
"Plugin 'scrooloose/nerdcommenter'

"node
Plugin 'moll/vim-node'

"rust
"Plugin 'rust-lang/rust.vim'

"tabline
"Plugin 'mkitt/tabline.vim'

"tagbar
"Plugin 'majutsushi/tagbar'

nmap <F8> :TagbarToggle<CR>

"buftabline
"Plugin 'ap/vim-buftabline'

"let g:buftabline = 1
"let g:buftabline_numbers = 1
"let g:buftabline_indicators = 1

set hidden

"tlib_vim
"Plugin 'tomtom/tlib_vim'
"Plugin 'vim-scripts/tlib'

"undotree
"Plugin 'mbbill/undotree'

"unicode
Plugin 'chrisbra/unicode.vim'

"unite
"Plugin 'Shougo/unite.vim'
"let g:unite_source_history_yank_enable = 1
"try
"  let g:unite_source_rec_async_command='ag --nocolor --nogroup -g ""'
"  call unite#filters#matcher_default#use(['matcher_fuzzy'])
"catch
"endtry

"Plugin 'rking/ag.vim'

"vim-addon-mw-utils
"Plugin 'MarcWeber/vim-addon-mw-utils'

"airline
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme="dark"

set laststatus=2

"go
"Plugin 'fatih/vim-go'

"handlebars

"indent-guides
Plugin 'nathanaelkane/vim-indent-guides'

set ts=4 sw=4

"jade
"Plugin 'digitaltoad/vim-jade'

"javascript
"Plugin 'pangloss/vim-javascript'

"let g:javascript_enable_domhtmlcss = 1

"javascript-syntax
"Plugin 'jelera/vim-javascript-syntax'

"pythonsyntax
"Plugin 'hdima/python-syntax'

"jsbeautify
"Plugin 'maksimr/vim-jsbeautify'
"Plugin 'einars/js-beautify'

"json
"Plugin 'elzr/vim-json'

"latex
"Plugin 'lervag/vimtex'

"tabular
"Plugin 'godlygeek/tabular'

"markdown
"Plugin 'plasticboy/vim-markdown'

"let g:vim_markdown_math=1

"let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']

"bundler
"Plugin 'tpope/vim-bundler'

"css
"Plugin 'lepture/vim-css'
"Plugin 'JulesWang/css.vim'

"css3-syntax

"Plugin 'hail2u/vim-css3-syntax'

"css-color
"Plugin 'skammer/vim-css-color'

"less
"Plugin 'groenewege/vim-less'

"easymotion
"Plugin 'easymotion/vim-easymotion'

let g:EasyMotion_keys='asdfqwerzxc1234 hgyvuiopnm57890,qwertzxcbasdjfkl;'

"vim-easy-align
"Plugin 'junegunn/vim-easy-align'
vnoremap <silent> <Enter> :EasyAlign<cr>

"git
"Plugin 'motemen/git-vim'

"gitgutter
Plugin 'airblade/vim-gitgutter'

let g:gitgutter_highlight_lines = 0

"matchit
"Plugin 'tmhedberg/matchit'

"rails
"Plugin 'tpope/vim-rails'

"vimproc
"Plugin 'Shougo/vimproc.vim'

"ruby
"Plugin 'vim-ruby/vim-ruby'

"sensible
"Plugin 'tpope/vim-sensible'

"slim
"Plugin 'slim-template/vim-slim'

"stylus
"Plugin 'vim-scripts/vim-stylus'
"Plugin 'wavded/vim-stylus'

"polyglot
"Plugin 'sheerun/vim-polyglot'

"Plugin 'svermeulen/vim-easyclip'

"Plugin 'tpope/vim-commentary'

"Plugin 'tpope/vim-abolish'

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line



"""" ENDOF VUNDLE



" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

if has("win32") || has("win64")
  "Required by vim-latex on windows machines
  " set shellslash
  "Otherwise vim will try to write temp files in sys32 folder when editing new file
  "set directory=.,$TEMP
else
  "set background=dark
  let g:solarized_termtrans=1
  let g:solarized_termcolors=256
  let g:solarized_contrast="normal"
  let g:solarized_visibility="normal"

  if !has("gui_running")
    set t_co=256
    colorscheme koehler
  else
    let g:rehash256 = 1
    set guifont=Ubuntu\ Mono\ 12
    colorscheme koehler
    let g:molokai_original = 1
  end
  if $TERM == 'linux'
    set t_co=256
    colorscheme koehler
  end
end

if (exists('+colorcolumn'))
  set colorcolumn=80
  highlight ColorColumn ctermbg=9
endif

" Fix for my shell, otherwise some scripts break
"if $SHELL =~ 'bin/fish'
"    set shell=/bin/sh
"endif

"nmap <s-j> :tabprevious<CR>
"nmap <s-k> :tabnext<CR>
"nmap <s-t> :tabe<Space>

nmap <silent> <s-tab> :bprevious<CR>
nmap <silent> <tab> :bnext<CR>
"nmap <silent> gT :tabprevious<CR>
"nmap <silent> gt :tabnext<CR>
nmap <silent> gT :bprevious<CR>
nmap <silent> gt :bnext<CR>

"nmap <s-t> :tabe<Space>

" backup to ~/.tmp 
"set backupdir=/tmp/.vimback
"set backupskip=/tmp/*,/private/tmp/* 
"set directory=/tmp/.vim
"set writebackup"
"set noswapfile

syntax on         " Enables syntax highlighting with custom colors
nmap § :buffer 

set clipboard^=unnamed,unnamedplus
"set clipboard=unnamed,unnamedplus
set showtabline=1
set history=100
"set scrolloff=2  " Minimum number of lines to display around cursor
set scrolljump=2 " Minimum jump distance
"set autoread	    " Files changed from outside are automatically reread
set hlsearch      " Highlight search results
set mousehide     " Hide the mouse when typing text
set smarttab      " <TAB> inserts 'shiftwidth' spaces
set shiftwidth=2	" Amount of spaces for tab to insert
set softtabstop=2 " ?
set autoindent    " Automatically set the indent when creating new lines.
set showcmd       " Shows current command in statusline
set ruler         " Show cursor position information in statusline
set number	      " Show absolute line number of current line
set foldenable
set nu            " Line numbers on
set smartindent
set tabstop=2
set enc=utf-8
set fileencoding=utf-8
set wrap          " Wrap text
set ttyfast       " 'Smooth' scrolling
set showmatch     " Briefly display matching brackets when inserting such.
set incsearch     " Incremental searching as soon as typing begins.
set ignorecase    " Ignores case when searching
set smartcase     " Will override ignorecase if searching w/ diff cases.
set modeline	    " Use modelines
set expandtab	    " Makes <tab> insert spaces in insert mode
set wildchar=<TAB> " Key that triggers command-line expansion.
set wildmenu
"set wildmode=longest:full
set wildmode=list:longest,full
set novisualbell
set noerrorbells " Disables beeping.
set hidden	" Allow switch buffer without saving
set switchbuf=useopen " If switching to a buffer that is already open, go
" to where it is already open instead of here.
set backspace=indent,eol,start whichwrap+=<,>,[,] "backspace functionality
set formatoptions=crolj " auto formatting options
" c - autowrap using textwidth
" r - autoinsert comment leader on i_<enter>
" l - long lines aren't broken
" j - remove comment leader when joining lines
"set noea	" prevent equalizing of split sizes on closed split
set ea

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pdf  " MacOSX/Linux
"set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe  " Windows
